# TensorFlowAI
TensorFlow project for AI with image classification and word embedding

## How to run
To run the program you need to use this command from the main folder:

```
python scripts/main.py
```

You can add also the following options:
```
  -h, --help           show this help message and exit
  --image IMAGE        image to be processed
  --text TEXT          text used for embedding
  --result RESULT      file where to store results
  --training TRAINING  is training needed? (Y or N)
  --n_words N_WORDS    number of near words to search
```
