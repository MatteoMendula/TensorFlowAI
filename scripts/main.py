import word_embedding_training as wet
import label_image as li
import argparse

if __name__ == '__main__':

    image = "daisy.jpg"
    text_filename = "files/text"
    result_filename = "files/results"
    training = True
    n_words = 5

    parser = argparse.ArgumentParser()
    parser.add_argument("--image", help="image to be processed")
    parser.add_argument("--text", help="text used for embedding")
    parser.add_argument("--result", help="file where to store results")
    parser.add_argument("--training", help="is training needed? (Y or N)")
    parser.add_argument("--n_words", help="number of near words to search")
    args = parser.parse_args()

    if args.image:
        image = args.image
    if args.text:
        text_filename = args.text
    if args.result:
        result_filename = args.result
    if args.training:
        training = (False, True)[args.training is 'Y']
    if args.n_words:
        near_words = args.n_words

    _, _, top_k, _, labels = li.get_labels(image)

    best_label = labels[top_k[0]]

    print("The best label given by the image classifier to " + image + " is " + best_label)

    (vectors, words) = wet.word_embedding(text_filename, result_filename, training)
    results = wet.get_near_words(best_label, n_words, vectors, words)
    print("\nThe " + str(5) + " nearest words to " + labels[0] + " are:")
    for i in range(5):
        print(results[i])

    wet.plot(vectors, words, with_nearest_words=True, target_word=best_label, nearest_words=results)